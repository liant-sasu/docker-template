# Docker Template

This is a simple template project with a working generic pipeline to push docker built
images to both [DockerHub](https://hub.docker.com) and the local gitlab.com registry
of the project itself.

## Reference links

- [GitLab CI Documentation](https://docs.gitlab.com/ee/ci/)
- [Docker](https://www.docker.com)
- [Docker Documentation](https://docs.docker.com)

If you are new to docker, please use [stack_overflow](https://stackoverflow.com) and
Docker's documentation.

## What's contained in this project

We provide a simple Hello World docker image running on several architectures and
severals linux distribution.

## Usage

To use this, update the Dockerfile and add the tags/architectures/base-images you want
in the file ./images.yml.

The images are made and deployed on Gitlab Registry during build stage. The deploy stage
is usefull only to deploy the image on an other Registry (like DockerHub)

### Docker HUB

To deploy on Docker Hub, this will be done only on `deploy` job and if DOCKER_TOKEN,
DOCKER_BASE_NAME (basically where your token has the right to deploy) environment
variables are set.

## Support

First get support on [stack_overflow](https://stackoverflow.com), you can tag
me [@rlaures](https://stackoverflow.com/users/3129859/rlaures) or recruit me or
my team.

## Contributing

See [Contribution file](./CONTRIBUTING.md) to contribute (basically, create an issue
[here](https://gitlab.com/liant-sasu/docker-template/-/issues/new) and be clear).

## Authors and acknowledgment

Contributors are from [Liant](https://gitlab.com/liant-sasu) (at this moment).

## License

[MIT](./LICENSE)
