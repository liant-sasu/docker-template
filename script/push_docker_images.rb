#! /usr/bin/env ruby
# frozen_string_literal: true

require_relative 'tools'
require_relative 'images'
require_relative 'registries'

needed_variables = %w[
  CI_REGISTRY CI_REGISTRY_IMAGE CI_REGISTRY_PASSWORD CI_REGISTRY_USER
]
unless needed_variables.all? { |variable| ENV.include?(variable) }
  warn 'Running outside Gitlab CI/CD Pipeline.'
  warn 'It can work for your tests, but you have to define at least those environment variables:'
  needed_variables.reject { |v| ENV.include?(v) }.each do |variable|
    warn "  - #{variable}"
  end
  exit 1
end

# First get back the images
run_system('Login to Gitlab CI registry') do
  regctl_login_on('CI_REGISTRY')
end

if ENV['REGISTRIES'].nil?
  warn 'Please define REGISTRIES where to push alow with their variables (see gitlab-ci.yml and README file)'
  exit 1
end

ENV['REGISTRIES'].split(' ').each do |registry_name|
  run_system("Login to '#{registry_name}' registry") do
    regctl_login_on(registry_name)
  end

  IMAGE_DESCRIPTIONS.each do |image, tags|
    puts '---------------'
    puts "#{image}:"

    tags.each_key do |tag|
      puts '---------------'
      puts "  #{tag}:"
      puts '---------------'

      # retag from CI_REGISTRY to destination registry
      local_images = tag_params(image, tag, path_to_image_for('CI_REGISTRY'))
      cmds = tag_params(image, tag, path_to_image_for(registry_name)).map.each_with_index do |ci_image, i|
        "regctl image copy #{local_images[i]} #{ci_image}"
      end
      run_system('Copy images', cmds)
    end
  end
end
